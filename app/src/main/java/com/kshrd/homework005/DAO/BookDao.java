package com.kshrd.homework005.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.kshrd.homework005.Entitiy.BookEntity;
import com.kshrd.homework005.Entitiy.CategoryEntity;

import java.util.List;

@Dao
public interface BookDao {

    //Category

    @Insert
    void insertCategory(CategoryEntity... category);

    @Transaction
    @Query("SELECT * FROM tb_category")
    List<CategoryEntity> getCategory();

    @Query("SELECT * FROM tb_category where category = :category")
    CategoryEntity getOneCategory(String category);

    @Query("SELECT * FROM tb_category where categoryId = :categoryId")
    CategoryEntity getOneCategory(int categoryId);

    //Book

    @Transaction
//    @Delete
//    void deleteBook(int bookId);

    @Query("SELECT * FROM tb_book")
    List<BookEntity> getBook();

    @Insert
    void insertBook(BookEntity... book);

    @Update
    void updateBook(BookEntity... book);

    @Delete
    void deleteBook(BookEntity... book);

}
