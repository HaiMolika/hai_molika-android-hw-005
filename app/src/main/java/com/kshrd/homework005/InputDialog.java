package com.kshrd.homework005;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kshrd.homework005.Entitiy.BookEntity;
import com.kshrd.homework005.Entitiy.CategoryEntity;
import com.kshrd.homework005.Room.BookDB;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class InputDialog extends DialogFragment {

    private static final int PERMISSION_REQUEST_CODE = 100;
    EditText title, size, price;
    Spinner category;
    ImageView image;
    FloatingActionButton load_img;
    Uri imageUri;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, 0);
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_input, null);

        title = view.findViewById(R.id.book_title);
        size = view.findViewById(R.id.book_size);
        price = view.findViewById(R.id.book_price);
        category = view.findViewById(R.id.category);
        image = view.findViewById(R.id.book_img);
        load_img = view.findViewById(R.id.load_img);

        //spinner

//        insertCategory();
//        getCategory();

        ArrayList<String> categoryList = new ArrayList<>();
        List<CategoryEntity> categoryEntities = getCategory();
        for (CategoryEntity category : categoryEntities){
            categoryList.add(category.getCategory());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, categoryList);
        category.setAdapter(arrayAdapter);

        //load image

        load_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });


        builder.setView(view)
                .setTitle("Insert Book")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handleOkButton();
                    }
                });

        alertDialog = builder.create();

        return alertDialog;
    }

    void handleOkButton(){

        CategoryEntity mCategory = BookDB.getInstance(getContext()).bookDao().getOneCategory(category.getSelectedItem().toString());
        BookEntity book = new BookEntity(
                title.getText().toString(),
                Float.parseFloat(size.getText().toString()),
                Float.parseFloat(price.getText().toString()),
                mCategory.getCategoryId(),
                imageUri
        );
        BookDB.getInstance(getContext()).bookDao().insertBook(book);
    }

    void loadImage(){
        if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
        else{
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_REQUEST_CODE){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PERMISSION_REQUEST_CODE);
            }else {
                Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == PERMISSION_REQUEST_CODE){
            if(data != null){
                imageUri = data.getData();
                image.setImageURI(imageUri);
            }
        }
    }

    void insertCategory(){
        BookDB.getInstance(getContext()).bookDao().insertCategory(new CategoryEntity("Sell"));
        BookDB.getInstance(getContext()).bookDao().insertCategory(new CategoryEntity("Donate"));
    }
    private List<CategoryEntity> getCategory(){
        List<CategoryEntity> categoryEntities;
        categoryEntities =  BookDB.getInstance(getContext()).bookDao().getCategory();
        Log.d("TAG", categoryEntities.toString());
        return categoryEntities;
    }

}
