package com.kshrd.homework005.Entitiy;

import android.net.Uri;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

@Entity(tableName = "tb_book")
public class BookEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private float size;
    private float price;

    @ForeignKey(
            entity = CategoryEntity.class,
            parentColumns = "categoryd",
            childColumns = "category_id"
    )
    private int category_id;
    private Uri image;

    public BookEntity(String title, float size, float price, int category_id, Uri image) {
        this.title = title;
        this.size = size;
        this.price = price;
        this.category_id = category_id;
        this.image = image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }
}
