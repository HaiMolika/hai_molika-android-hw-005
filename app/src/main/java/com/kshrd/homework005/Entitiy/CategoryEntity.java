package com.kshrd.homework005.Entitiy;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_category")
public class CategoryEntity {

    @PrimaryKey(autoGenerate = true)
    private int categoryId;
    private String category;

    public CategoryEntity() {

    }

    public CategoryEntity(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
