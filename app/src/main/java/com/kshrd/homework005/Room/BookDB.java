package com.kshrd.homework005.Room;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.kshrd.homework005.DAO.BookDao;
import com.kshrd.homework005.Entitiy.BookEntity;
import com.kshrd.homework005.Entitiy.CategoryEntity;
import com.kshrd.homework005.Entitiy.UriConverter;

@Database(entities = {CategoryEntity.class, BookEntity.class}, version = 1)
@TypeConverters({UriConverter.class})
public abstract class BookDB extends RoomDatabase {

    public abstract BookDao bookDao();
    private static BookDB bookDB;
    public static BookDB getInstance(Context context){
        if(bookDB == null ){
            bookDB = Room.databaseBuilder(context.getApplicationContext(), BookDB.class, "db_book")
                        .allowMainThreadQueries()
                        .build();
        }
        return bookDB;
    }

}
