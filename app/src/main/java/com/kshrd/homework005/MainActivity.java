package com.kshrd.homework005;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity
                            implements BottomNavigationView.OnNavigationItemSelectedListener
{

    private static final String TAG = "MainActivity";

    EditText title, size, price;
    Spinner category;
    ImageView image;
    FloatingActionButton load_img;

    BottomNavigationView bottomNavigationView;
    HomeFragment home;
    DashboardFragment dashboard;
    NotificationFragment notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        home = new HomeFragment();
        dashboard = new DashboardFragment();
        notification = new NotificationFragment();

        getSupportActionBar().setTitle("Home");
        getSupportFragmentManager().beginTransaction().add(R.id.container, home).commit();
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.search);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.home :
                getSupportFragmentManager().beginTransaction().replace(R.id.container, home).commit();
                getSupportActionBar().setTitle("Home");
                return true;
            case R.id.dashboard :
                getSupportFragmentManager().beginTransaction().replace(R.id.container, dashboard).commit();
                getSupportActionBar().setTitle("Dashboard");
                return true;
            case R.id.notification :
                getSupportFragmentManager().beginTransaction().replace(R.id.container, notification).commit();
                getSupportActionBar().setTitle("Notification");
                return true;
            default:
                Log.i(TAG, "Another item is click!");
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.add :
                openDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openDialog(){
        InputDialog inputDialog = new InputDialog();
        inputDialog.show(getSupportFragmentManager(), "tag");
    }
}