package com.kshrd.homework005;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kshrd.homework005.Entitiy.BookEntity;
import com.kshrd.homework005.Entitiy.CategoryEntity;
import com.kshrd.homework005.Room.BookDB;

import java.util.List;
import java.util.Map;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ItemViewHolder>{

    private List<BookEntity> mItemList;
    private Context context;


    public BookAdapter(List<BookEntity> mItemList, Context context){
        this.mItemList = mItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_book, parent, false);

        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        CategoryEntity categoryEntities = BookDB.getInstance(context).bookDao().getOneCategory(mItemList.get(position).getCategory_id());

        holder.txttitle.setText(mItemList.get(position).getTitle().toString());
        holder.txtsize.setText(mItemList.get(position).getSize()+"");
        holder.txtprice.setText(mItemList.get(position).getPrice()+"");
        holder.txtcategory.setText(categoryEntities.getCategory());
        holder.imgbook.setImageURI(mItemList.get(position).getImage());

        holder.imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, holder.imgmenu);
                popupMenu.inflate(R.menu.book);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.add:
                                FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                                InputDialog inputDialog = new InputDialog();
                                inputDialog.show(fragmentManager, "Tag");
                                return true;
                            case R.id.edit:
                                return true;
                            case R.id.delete :
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private TextView txttitle, txtcategory, txtsize, txtprice;
        ImageView imgbook, imgmenu;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.book);
            txttitle = itemView.findViewById(R.id.book_info_title);
            txtcategory = itemView.findViewById(R.id.book_info_category);
            txtsize = itemView.findViewById(R.id.book_info_size);
            txtprice = itemView.findViewById(R.id.book_info_price);
            imgbook = itemView.findViewById(R.id.book_info_img);
            imgmenu = itemView.findViewById(R.id.manipulate_book);



        }
    }

}
