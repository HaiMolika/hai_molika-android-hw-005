package com.kshrd.homework005;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kshrd.homework005.Entitiy.BookEntity;
import com.kshrd.homework005.Entitiy.CategoryEntity;
import com.kshrd.homework005.Room.BookDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
  
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = view.findViewById(R.id.book_container);
        List<BookEntity> bookList = BookDB.getInstance(getContext()).bookDao().getBook();
        BookAdapter bookAdapter = null;
        if (bookList !=null){
            bookAdapter = new BookAdapter(bookList, getContext());
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2 );
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(bookAdapter);
        return view;
    }

}